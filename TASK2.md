## Git completed tasks
1. Create a gitlab account (done)
2. Create a new remote repository (done)
3. Install git on local machine (done)
4. Initialize local repository (done)
5. Link local repository to remote (done)
6. Describe the previously performed steps in the form of TASK1.MD file indicating the executed commands
7. Commit the file to the local repository
8. Push to the remote repository. Go  to the gitlab repository page and make sure the changes appear

Задание провалено!:(

9. Add steps 7-8 in file
10. Call commands diff & status
11. Commit changes, check status
12. Make push to remote repo

Задание успешно выполнено!:)

1.Шаги 10-12 описать в новом файле TASK2.MD
2.Отвести бранч develop, в который залить файл TASK2.MD. Локально и удаленно.
3.Добавить в конец файла TASK1.MD строчку "Задание успешно выполнено!:)". Залить изменения локально и удаленно.
4.Переключиться назад на основной бранч
5.Добавить в файл TASK1.MD строчку "Задание провалено!:(". Залить изменения локально и удаленно.
6.Выполнить мерж из ветки develop в основную верку. Разрешить конфликт в пользу ветки develop
7.Посмотреть лог проделанных изменений
